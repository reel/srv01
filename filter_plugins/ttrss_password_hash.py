#Source: https://github.com/marklee77/ansible-role-ttrss
#Author: Mark Stillwell
#License: GPLv2

def ttrss_password_hash(*a, **kw):
    from hashlib import sha256
    password = a[0].encode('utf-8')
    salt = a[1].encode('utf-8')
    password_hash = sha256(salt + password).hexdigest()
    return 'MODE2:' + password_hash

class FilterModule(object):
    ''' utility filter hash passwords '''

    def filters(self):
        return { 'ttrss_password_hash' : ttrss_password_hash }
