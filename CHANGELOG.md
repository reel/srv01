# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).


## [v0.10.0](https://github.com/nodiscc/dbu/releases/tag/v0.9.0) - 2019/04/25

Changes since v0.9.9:

Full refactoring:

 - Rewrite roles to be compatible with standard ansible playbooks, add role metadata
 - Move all config variables to role defaults, start with a very basic config file (mandatory settings such as usernames/password)
 - Simplify requirements
 - Let ansible handle password generation
 - Move config/user data to a single data/ directory
 - Remove mandatory use of tags, the server can now be reconfigured with just ./srv01
 - Single command installation (almost)
 - Rewrite main script to Python
 - Update docs
 - Refactor tests (syntax checks and linting)
 - Remove reporting script in preparation of netdata role
 - Make most of the playbook idempotent
 - Remove obsolete components, handlers, tasks...
 - Better separation of components in common role


Migration path from previous versions:

 - `srv01 backup-force; ./srv01 backup-sync`, snapshot of the VM
 - `git pull`
 - Copy srv01-config.dist.yml to data/config/your.domain.name.yml
 - Copy `srv01-{playbook,inventory}.yml.dist` to `data/config/srv01-{playbook,inventory}.yml`
 - Replace the domain name in both files, enable/disable roles
 - Copy credentials from config.yml to data/config/your.domain.name.yml
 - Delete config.yml
 - Move `keys/id_rsa*` to `config/secrets/your.domain.name.id_rsa*`
 - Move `backups/` to `data/`
 - `ln -s data/config/your.domain.name.yml data/config/srv01-config.yml`
 - `ln -s data/config/srv01-playbook.yml srv01-playbook.yml`
 - `./srv01`

<!-- ### Added

### Changed

### Removed

### Fixed

### Security

### Deprecated -->


------------------------------------------------------------------

## Before v0.9.9

This project started a while ago as a bunch of shell scripts to deploy local servers.
Then it was migrated to ansible roles using a custom bash command-line wrapper.
It has endured many refactorings and most of it can still be found in the git history
(the first commit is from may 2015). Run `gource --hide filenames,bloom` for a quick
visual history of this project.