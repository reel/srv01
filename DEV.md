
#### Release procedure

- [ ] `git checkout master`
- [ ] `make tests`
- [ ] `srv01 setup`
- [ ] `srv01 setup` again to test idempotency
- [ ] `git checkout stable`
- [ ] `git merge master`
- [ ] `make tests`
- [ ] `git push --all`