#!/usr/bin/env python3
""" initial config file creation and pre-install checks for srv01 """
import os
import re
import socket
from argparse import ArgumentParser
import yaml
import pprint; pp = pprint.PrettyPrinter(indent=4)#; pp.pprint(my_dict)


""" This adds:
- An interactive setup wizard for https://gitlab.com/nodiscc/srv01
Point it to an SSH server + user in the sudo group
Basic connectivity checks will be made, then a set of default config
files (ansible playbook, inventory and host_vars file) will be
generated from the answers.
- A wrapper around ansible-playbook using hardcoded vars/inventory/
playbook file paths.
"""

SRV01_PATH = os.path.dirname(os.path.realpath(__file__))
INVENTORY_FILE_DIST = SRV01_PATH + '/srv01-inventory.yml.dist'
INVENTORY_FILE = SRV01_PATH + '/data/config/srv01-inventory.yml'
PLAYBOOK_FILE_DIST = SRV01_PATH + '/srv01-playbook.yml.dist'
PLAYBOOK_FILE = SRV01_PATH + '/data/config/srv01-playbook.yml'
PLAYBOOK_FILE_LINK = SRV01_PATH + '/srv01-playbook.yml'
CONFIG_FILE_DIST = SRV01_PATH + '/srv01-config.yml.dist'
CONFIG_FILE = SRV01_PATH + '/data/config/srv01-config.yml'
HTML_DATA_FILE = SRV01_PATH + '/data/srv01.html'
BANNER = """
    ░░░░░█▀█ ┏━┓┏━┓╻ ╻┏━┓╺┓ 
    ░░░░░█░█ ┗━┓┣┳┛┃┏┛┃┃┃ ┃ 
    ░▀▀▀░▀▀▀ ┗━┛╹┗╸┗┛ ┗━┛╺┻╸
"""

def ask_yes_no(default, msg):
    """ Read y/n answer, return True/False, or the provided default value if empty """
    answer = 'undefined'
    while answer not in ['y', 'Y', 'N', 'n', '']:
        answer = input(msg)
        if answer in ['y', 'Y']:
            return True
        if answer in ['n', 'N']:
            return False
        if answer == '':
            return default

def resolve(fqdn):
    """ check if a fqdn can be resolved to an IP address """
    print('[srv01] INFO: Trying to resolve {} ...'.format(fqdn))
    try:
        socket.gethostbyname(fqdn)
    except socket.gaierror:
        print("[srv01] WARNING: Failed to resolve {} (DNS lookup failed)".format(fqdn))
        return False
    return True

def write_etc_hosts(server_ip, fqdn):
    """ add an entry to /etc/hosts """
    print('[srv01] INFO: adding entry for {} ({}) to /etc/hosts ...'.format(fqdn, server_ip))
    etchosts_line = '#line added by srv01\n{}    {}\n'.format(server_ip, fqdn)
    with open('/etc/hosts', 'r') as hosts_file, open('.tmp.hosts', 'w+') as tmp_hosts_file:
        hosts = hosts_file.read() + etchosts_line
        tmp_hosts_file.write(hosts)
    if not os.system('sudo mv .tmp.hosts /etc/hosts') == 0:
        exit(1)

def check_ping(address):
    """ check if a remote host responds to ping """
    print("[srv01] INFO: Sending ping to {} ...".format(address))
    if not os.system('ping -c1 {}'.format(address)) == 0:
        return False
    return True

def test_ssh(address, port):
    """ test if a tcp connection can be established to a remote host/port """
    print("[srv01] INFO: Checking SSH port {} ...".format(port))
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(1)
    test = sock.connect_ex((address, port))
    if test == 0:
        print("[srv01] INFO: OK: Port tcp/{} is open.".format(port))
        return True
    return False

def get_password(msg):
    """ ask for a password and confirm it"""
    password = ''
    while password == '':
        password = input(msg + ": ")
    password_confirm = input(msg + " (confirm): ")
    if not password == password_confirm:
        print('[srv01] ERROR: passwords do not match.')
        exit(1)
    return password

def gen_ssh_key(password, key_file):
    """ generate a SSH RSA key pair"""
    print('[srv01] INFO: generating ssh key in {} ...'.format(key_file))
    gen_ssh_command = 'ssh-keygen -q -t rsa -b 4096 -f {} -N {}'.format(key_file, password)
    if not os.system(gen_ssh_command) == 0:
        print('[srv01] ERROR: error generating ssh key')
        exit(1)

def ssh_copy_id(key_file, ssh_port, username, address):
    """ authorize SSH key on remote host, ask to validate host keys """
    print('[srv01] INFO: authorizing SSH keys on remote host ...')
    ssh_command = 'ssh-copy-id -i {} -p {} {}@{}'.format(key_file, ssh_port, username, address)
    if not os.system(ssh_command) == 0:
        print('[srv01] ERROR: error authorizing SSH key')
        exit(1)

def write_config(fqdn, ssh_port, admin_email, services_username,
                 services_password, sudo_username, sudo_password, host_vars_file):
    """ write initial/base config files"""
    print('[srv01] INFO: Writing configuration files ...')

    # inventory file
    with open(INVENTORY_FILE_DIST, 'r') as inventory_dist:
        inventory_data = re.sub('example.net', fqdn, inventory_dist.read())
    with open(INVENTORY_FILE, 'w+') as inventory:
        inventory.write(inventory_data)

    # playbook file
    with open(PLAYBOOK_FILE_DIST, 'r') as playbook_dist:
        playbook_data = re.sub('example.net', fqdn, playbook_dist.read())
    with open(PLAYBOOK_FILE, 'w+') as playbook:
        playbook.write(playbook_data)
    os.symlink(PLAYBOOK_FILE, PLAYBOOK_FILE_LINK)


    # config/host_vars file
    with open(host_vars_file, 'w+') as config_host:
        config_host.write('#### HOST CONFIGURATION FILE ###\n'
                          '# DNS name of the host (Fully Qualified Domain Name)\n')
        config_host.write('host_fqdn: "{}"\n'.format(fqdn))

        config_host.write('# username/password for the administrator (sudo) account\n')
        config_host.write('admin_username: "{}"\n'.format(sudo_username))
        config_host.write('admin_password: "{}"\n'.format(sudo_password))

        config_host.write('# server administrator e-mail address\n')
        config_host.write('admin_email: "{}"\n'.format(admin_email))

        config_host.write('# username/password for default admin account '
                          'created on your services/web applications...\n')
        config_host.write('services_username: "{}"\n'.format(services_username))
        config_host.write('services_password: "{}"\n'.format(services_password))

        config_host.write('# Port for initial SSH connection (before first deployment)\n')
        config_host.write('init_ssh_port: "{}"\n'.format(ssh_port))
        config_host.write('# SSH private key\n')
        config_host.write('ansible_ssh_private_key_file: '
                          '"{{ playbook_dir }}/data/secrets/{{ host_fqdn }}.id_rsa"\n')
    os.symlink(host_vars_file, CONFIG_FILE)

def xdg_open_html_data_file():
    """ open the HTML shortcuts/passwords page in a web browser"""
    os.system('xdg-open ' + HTML_DATA_FILE)

def run_ansible(tags, initial):
    """ run ansible-playbook with appropriate options"""
    # pylint: disable=C0301,C0330
    if initial:
        os.system('ansible-playbook --inventory ' + INVENTORY_FILE + ' ' + PLAYBOOK_FILE_LINK +
                  ' --extra-vars "ansible_ssh_port={{ init_ssh_port }} ansible_ssh_pass={{ admin_password }}"')

        open_html_data_file = ask_yes_no(True,
              '[srv01] INFO: A HTML page containing shortcuts, usernames and '
              'passwords for your services has been created at {}. '
              'Do you want to open it now? [Y/n] '
              .format(HTML_DATA_FILE))
        if open_html_data_file:
            xdg_open_html_data_file()

    else:
        if tags:
            os.system('ansible-playbook --inventory ' + INVENTORY_FILE + ' ' + PLAYBOOK_FILE_LINK + ' --tags=' + tags)
        else:
            os.system('ansible-playbook --inventory ' + INVENTORY_FILE + ' ' + PLAYBOOK_FILE_LINK)


def interactive_initial_setup():
    """ interactive initial setup/config file generation """

    # abort if any config file already exists
    existing_config_files = []
    for file in [INVENTORY_FILE, CONFIG_FILE, PLAYBOOK_FILE, PLAYBOOK_FILE_LINK]:
        if os.path.isfile(file):
            existing_config_files.append(file)
    if existing_config_files:
        print('[srv01] ERROR: configuration files already exist: \n{}'
              .format(str.join('\n', existing_config_files)))
        exit(1)

    fqdn = input("DNS domain name (FQDN) for the server: ")
    ssh_key_file = SRV01_PATH + '/data/secrets/' + fqdn + '.id_rsa'
    host_vars_file = SRV01_PATH + '/data/config/' + fqdn + '.yml'

    if os.path.isfile(host_vars_file):
        print('[srv01] ERROR: configuration file already exists: host_vars_file')
        exit(1)

    if not resolve(fqdn):
        update_etchosts = ask_yes_no(False, 'Add an entry in /etc/hosts for {}? [N/y] '
                                     .format(fqdn))
        if not update_etchosts:
            print('[srv01] ERROR: IP address for host {} is unknown.'.format(fqdn))
            exit(1)
        else:
            server_ip = input("Please specify the IP address for {}: ".format(fqdn))
            write_etc_hosts(server_ip, fqdn)

    # check if server is reachable (must reply to ping)
    if check_ping(fqdn):
        print('[srv01] INFO: OK: ping succeeded.')
    else:
        print('[srv01] ERROR: ping to {} failed'.format(fqdn))
        exit(1)

    # try port tcp/22 by default, ask for another port if it fails
    if test_ssh(fqdn, 22):
        ssh_port = 22
    else:
        ssh_port = int(input("Connection to {} port 22 failed. "
                             "If you want to try another port, enter it now: ".format(fqdn)))
        if not test_ssh(fqdn, ssh_port):
            print('ERROR: cannot contact SSH server on {} port {}'.format(fqdn, ssh_port))
            exit(1)

    # get sudo user credentials
    sudo_username = input("Administrator (sudo) username on the server "
                          "(leave blank for default 'deploy'): ")
    if sudo_username == '':
        sudo_username = 'deploy'
    sudo_password = get_password("Administrator (sudo) password on the server")

    # generate and copy the ssh key to the host
    # key has the same passhprase as the user account
    if not os.path.isfile(ssh_key_file):
        gen_ssh_key(sudo_password, ssh_key_file)
    ssh_copy_id(ssh_key_file, ssh_port, sudo_username, fqdn)

    # define admin username/password/mail for services
    print("[srv01] INFO: Default admin accounts will be created "
          "for your services and web applications.")
    services_username = ''
    while services_username == '':
        services_username = input("Username for the default user account: ")

    # pylint: disable=C0301
    gen_services_password = ask_yes_no(True, "Generate a random password for the default account? [Y/n] ")
    if not gen_services_password:
        services_password = get_password("Default user account password: ")
    else:
        services_password = "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.services_password length=15') }}"

    admin_email = input("Administrator e-mail address [admin@localhost]: ")
    if admin_email == '':
        admin_email = 'admin@localhost'

    write_config(fqdn, ssh_port, admin_email, services_username,
                 services_password, sudo_username, sudo_password, host_vars_file)

    print('[srv01] INFO: Configuration files created.')
    print('[srv01] INFO: Edit files in the data/config/ directory to change configuration details.')
    print('[srv01] INFO: See https://github.com/nodiscc/srv01/doc/configuration-defaults.md')
    if ask_yes_no(True, "Run server deployment now with the default configuration? [Y/n] "):
        run_ansible('', initial=True)
    else:
        print('[srv01] INFO: After editing the configuration, '
              'run ./srv01 setup --initial to deploy your server.')

def load_config(configfile):
    with open(configfile, 'r') as f:
         data = yaml.load(f.read(), Loader=yaml.BaseLoader)
         config = data[0]
         with open(config['vars_files'][0], 'r') as vars_file:
             yaml_vars = yaml.load(vars_file.read(), Loader=yaml.BaseLoader)
         config.update(yaml_vars)
    return config

def interactive_ssh(config):
    private_key_file = "./data/secrets/{}.id_rsa".format(config['hosts'])
    os.system('ssh -p {} -i {} {}@{}'.format(config['ansible_ssh_port'], private_key_file,
                                             config['admin_username'], config['hosts']))
    # print(config)
    # print('{} -i {} {}@{}'.format(config['ansible_ssh_port'], private_key_file,
    #                               config['ansible_username'], ansible['hosts']))
    # debug
    pp.pprint(config)
    exit()

def main():
    """ Command line entrypoint """
    print(BANNER)

    parser = ArgumentParser()
    parser.add_argument(
        '--initial',
        help="First server deployment using password auth/default SSH port",
        action='store_true'
        )
    parser.add_argument(
        '--tags',
        help="Tags to pass to ansible-playbook (comma-separated)",
        default=None
        )
    parser.add_argument(
        '--shell',
        action='store_true',
        help="Open interactive shell on the server (SSH)"
        )

    args = parser.parse_args()


    if args.initial:
        run_ansible(tags=None, initial=True)
    elif os.path.isfile(PLAYBOOK_FILE):
        if args.shell:
            config = load_config(PLAYBOOK_FILE)
            interactive_ssh(config)
        run_ansible(args.tags, initial=False)
    else:
        interactive_initial_setup()

##########################

main()
