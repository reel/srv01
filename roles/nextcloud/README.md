nextcloud
=============

This role will install [Nextcloud](https://en.wikipedia.org/wiki/Nextcloud), a file hosting service and collaborative suite.

Basic functionality includes uploading, viewing, editing and downloading files from a convenient web interface, labeling files, and sharing them between users. It can be extended to a full groupware/collaborative suite/personal cloud by more than 200 [applications](https://apps.nextcloud.com/). 

Nextcloud [clients](https://nextcloud.com/clients/) can be installed on any computer (Linux/OSX/Windows) or mobile device (Android/iOS) and allows automatically synchronizing your files with the server.

Default installed applications include:

- [Contacts](https://apps.nextcloud.com/apps/contacts): Easily sync address books from various devices with your Nextcloud and edit them online, with addressbook sharing, integration with other apps, and CardDAV synchronization.
- [Calendar](https://apps.nextcloud.com/apps/calendar): Sync events from various devices with your Nextcloud and edit them online, with search, alarms, invitation management, contacts integration, CalDAV and ICS synchronization.
- [Tasks](https://apps.nextcloud.com/apps/tasks): Task/todo-list manager, with time management, reminders, priorities, comments, tasks sharing between users, CalDAV or ICS synchronization.
- [Music](https://apps.nextcloud.com/apps/music): Plays audio files directly or in a library view, with playlist management, track search, ampache support, and more.
- [Gallery](https://github.com/nextcloud/gallery): Media gallery with previews for all media types.
- [Talk](https://apps.nextcloud.com/apps/spreed): Video & audio-conferencing using WebRTC, with basic chat, private/group/public and password protected calls, screen sharing, integration with other Nextcloud apps.
- Viewers and editors for common file types (PDF, text, video...)
- Federation between Nextcloud instances (seamless acess to remote files/shares)
- Remote file storage access (FTP, SFTP, Samba/CIFS...).

Nextcloud is an alternative to services such as Dropbox, Google Drive/Agenda.

**[Homepage](https://www.nextcloud.com/)**


[![](https://i.imgur.com/vPzu9uR.png)](https://i.imgur.com/VBcngBu.png)
[![](https://i.imgur.com/SwKB9oO.png)](https://i.imgur.com/Z2Ipms0.png)
[![](https://i.imgur.com/dq8Qign.png)](https://i.imgur.com/3E6765w.jpg)
[![](https://i.imgur.com/HCkCrbK.png)](https://i.imgur.com/rddiC1i.png)
[![](https://i.imgur.com/Av6ofs5.png)](https://i.imgur.com/zEy39f9.png)
[![](https://i.imgur.com/2d0iyhb.png)](https://i.imgur.com/WTUIoSA.png)
[![](https://i.imgur.com/BI83IYv.png)](https://i.imgur.com/ZaQBCQ3.png)
[![](https://i.imgur.com/UiSY9B5.png)](https://i.imgur.com/GjJDXw5.png)
[![](https://i.imgur.com/y8iAUnH.png)](https://i.imgur.com/fYVWHRZ.png)
[![](https://i.imgur.com/l3Y1RRs.png)](https://i.imgur.com/oQ6JaWD.png)
[![](https://i.imgur.com/pfU5Ckb.png)](https://i.imgur.com/4O4Z1nM.png)



Clients
-----------

File synchronization:
 * [Desktop application (Linux/OSX/Windows)](https://nextcloud.com/install/#install-clients)
 * [Android app](https://f-droid.org/repository/browse/?fdid=com.nextcloud.android)
 * [iOS app](https://itunes.apple.com/us/app/nextcloud/id1125420102)

Calendar, contacts and tasks synchronization:
 * [Thunderbird](https://www.mozilla.org/en-US/thunderbird/) + [Lightning](https://www.mozilla.org/en-US/projects/calendar/) (Linux/OSX/Windows)
 * [DAVDroid](https://f-droid.org/repository/browse/?fdid=at.bitfire.davdroid) (Android)
 * [OpenTasks](https://f-droid.org/repository/browse/?fdid=org.dmfs.tasks) (Android)


Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)


Dependencies
------------

None

Example Playbook
----------------


License
-------

GNU GPLv3


References
----------------

https://gitlab.com/nodiscc/NOTES/blob/master/web-server.md#nextcloud