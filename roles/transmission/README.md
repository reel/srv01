transmission
=============

This role will install the [Transmission](https://en.wikipedia.org/wiki/Transmission_(BitTorrent_client)) Bittorrent client. [Bittorrent](https://en.wikipedia.org/wiki/BitTorrent) is a [Peer-to-peer](https://en.wikipedia.org/wiki/Peer-to-peer) file sharing network.

Transmission allows you to manage torrent downloads from a web interface. Torrents will be downloaded to the server, and can be accessed them using file transfer services (SFTP, Nextcloud...). This is also known as a [Seedbox](https://en.wikipedia.org/wiki/Seedbox).

Downloaded files can be accessed over SFTP in the `TORRENTS` directory.

[![](https://i.imgur.com/blWO4LL.png)](https://i.imgur.com/q1gcHRf.png)


Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)

Dependencies
------------

The `apache2` role

Example Playbook
----------------


License
-------

GNU GPLv3


References
-----------------

- https://github.com/transmission/transmission/wiki/Editing-Configuration-Files