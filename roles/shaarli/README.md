shaarli
=============

This role will install [Shaarli](https://shaarli.readthedocs.io/en/master/), a minimalist bookmark manager and link sharing service.

It can be used to bookmark useful/frequent links and share them between computers, share/comment/save interesting links and news, as a blog/microblog/writing platform, a read-it-later list, to save articles/posts/ideas/notes/snippets/todo-lists, and more.

Links can be organized, searched using tags, full-text search, public/private status, permalinks, tag cloud/list views, RSS/ATOM feeds, daily digest, picture wall, and other useful features.

* [Github project](https://github.com/shaarli/shaarli)
* [Demo](https://demo.shaarli.org/)
* [Documentation](https://shaarli.readthedocs.io/en/master/)

[![](https://i.imgur.com/8wEBRSG.png)](https://i.imgur.com/WWPfSj0.png)
[![](https://i.imgur.com/rrsjWYy.png)](https://i.imgur.com/TZzGHMs.png)
[![](https://i.imgur.com/uICDOle.png)](https://i.imgur.com/27wYsbC.png)
[![](https://i.imgur.com/KNvFGVB.png)](https://i.imgur.com/0f5faqw.png)
[![](https://i.imgur.com/tVvD3gH.png)](https://i.imgur.com/zGF4d6L.jpg)
[![](https://i.imgur.com/8iRzHfe.png)](https://i.imgur.com/sfJJ6NT.png)
[![](https://i.imgur.com/GjZGvIh.png)](https://i.imgur.com/QsedIuJ.png)
[![](https://i.imgur.com/TFZ9PEq.png)](https://i.imgur.com/KdtF8Ll.png)
[![](https://i.imgur.com/IvlqXXK.png)](https://i.imgur.com/boaaibC.png)
[![](https://i.imgur.com/nlETouG.png)](https://i.imgur.com/Ib9O7n3.png)




Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)


Dependencies
------------

None

Example Playbook
----------------


License
-------

GNU GPLv3
