apache2
=============

This role will install the [Apache](https://en.wikipedia.org/wiki/Apache_HTTP_Server) [web server](https://en.wikipedia.org/wiki/Web_server), the [PHP](https://en.wikipedia.org/wiki/PHP) interpreter module, and MySQL/[MariaDB](https://en.wikipedia.org/wiki/MariaDB) database management system.

Apache allows you to serve web pages/applications over the network.

This role is a requirement for web application roles, it configures the default Apache site (VirtualHost), hardens various web server security settings.

[![](https://i.imgur.com/bpt8GiK.png)](https://i.imgur.com/rIP2Hxj.png)


### SSL/TLS certificates

The server only listens for HTTPS on port 443.

The default is to generate and use a self-signed certificate (this will generate alerts in browsers/clients).

To use a [Let's Encrypt](https://en.wikipedia.org/wiki/Let%27s_Encrypt) certificate, set `apache_cert_mode: 'letsencrypt'` in the host configuration.

For let's encrypt certificates to work, port tcp/443 must be reachable from the Internet, and the correct public DNS record must be pointing to your server. If the server is reachable only from LAN, use self-signed certificates.

Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)


Dependencies
------------

None

Example Playbook
----------------


License
-------

GNU GPLv3


References
-----------------

- https://gitlab.com/nodiscc/NOTES/blob/master/web-server.md
- https://gitlab.com/nodiscc/NOTES/blob/master/storage-filesystems-databases.md#databases