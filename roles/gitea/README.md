gitea
=============

This role will install the [Gitea](https://gitea.io/en-us/) self-hosted Git service.
Gitea is a lightweight code hosting solution written in Go. Gitea features include:

- [Git](https://en.wikipedia.org/wiki/Git) repository management (view, edit, log, blame, diff, merge, releases, branches...)
- Issue management/collaboration (labels, milestones, pull requests, assignees...)
- Dashboards, activity tracker
- Search in code, issues, repositories...
- Web based code editing tools, code highlighting, file upload
- Administration tools (user/instance management, keys/2FA authentication, permissions...)
- Organizations and teams
- Markdown rendering and code highlighting
- Wikis

**[Documentation](https://docs.gitea.io/en-us/)**

[![](https://i.imgur.com/Rks90zV.png)](https://i.imgur.com/2TGIshE.png)
[![](https://i.imgur.com/cBktctp.png)](https://i.imgur.com/EauaJxq.png)
[![](https://i.imgur.com/gvcfs6G.png)](https://i.imgur.com/DHku4ke.png)
[![](https://i.imgur.com/4NhXqdG.png)](https://i.imgur.com/d5glB4P.png)

Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)


Dependencies
------------

`apache2` role

Example Playbook
----------------

```yaml
- hosts: srv01
  vars_files:
    - versions.yml
    - config.yml
  gather_facts: True
  become: True
  become_user: root
  roles:
    - common
    - apache2
    - gitea
```

License
-------

GNU GPLv3

References
-------------

https://gitlab.com/nodiscc/NOTES/blob/master/git.md#gitea