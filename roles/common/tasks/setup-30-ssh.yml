---

##### SSH SERVER #####

- name: copy sshd config
  template: src=etc_ssh_sshd-config.j2 dest=/etc/ssh/sshd_config
  notify: restart ssh

- name: install SSH packages
  apt:
    state: present
    update_cache: yes
    cache_valid_time: 900
    package:
      - openssh-server
      - pinentry-curses


# Mitigate logjam attack
- name: Check if /etc/ssh/moduli contains weak DH parameters
  shell: awk '$5 < 2048' /etc/ssh/moduli
  register: sshd_register_moduli
  changed_when: false

- name: remove all small primes
  shell: awk '$5 >= 2048' /etc/ssh/moduli > /etc/ssh/moduli.new ;
         [ -r /etc/ssh/moduli.new -a -s /etc/ssh/moduli.new ] && mv /etc/ssh/moduli.new /etc/ssh/moduli || true
  notify: restart ssh
  when: sshd_register_moduli.stdout

- name: create sftponly group
  group: name=sftponly state=present

- name: create base SFTP data directory (setgid)
  file: path=/var/lib/sftp state=directory owner=root group=sftponly mode=02750


##### SFTP ACCOUNTS #####

- name: create SSH keypairs for SFTP users
  delegate_to: localhost
  command: ssh-keygen -t rsa -f {{ playbook_dir }}/data/secrets/{{ host_fqdn }}.id_rsa-{{ item.name }} -q -N '{{ item.key_passphrase }}'
  become: False
  with_items: "{{ sftp_users }}"
  args:
    creates: "{{ playbook_dir }}/data/secrets/{{ host_fqdn }}.id_rsa-{{ item.name }}"

# note: this task will report changed every time because a random password is generated
- name: add SFTP user accounts
  user:
    name: '{{ item.name }}'
    state: present
    createhome: no
    group: sftponly
    groups: ''
    shell: /bin/false
    home: '/var/lib/sftp/'
    generate_ssh_key: no
    password: "{{ lookup('password', '/dev/null length=15 chars=ascii_letters,digits') }}"
  with_items: "{{ sftp_users }}"

- name: authorize SSH keys for SFTP users
  authorized_key:
    exclusive: no
    manage_dir: yes
    state: present
    key: "{{ lookup('file', '{{ playbook_dir }}/data/secrets/{{ host_fqdn }}.id_rsa-{{ item.name }}.pub') }}"
    user: "{{ item.name }}"
  with_items: "{{ sftp_users }}"
  notify: display sftp account creation completion message


##### SSH CLIENT (LOCAL) #####

- name: find current $HOME on the local machine (controller)
  set_fact:
    local_home: "{{ lookup('env','HOME') }}"

- name: check whether gtk bookmarks file exists (on the controller)
  become: False
  delegate_to: localhost
  stat: path='{{ local_home }}/.config/gtk-3.0/bookmarks'
  register: gtk_bookmarks_exist

- name: add SFTP server to gtk bookmarks file (on the controller)
  become: False
  delegate_to: localhost
  when: gtk_bookmarks_exist
  lineinfile:
    create: no
    state: present
    dest: '{{ local_home }}/.config/gtk-3.0/bookmarks'
    line: 'sftp://{{ admin_username }}@{{ host_fqdn }}/home/{{ admin_username }} {{ host_fqdn }}'

- name: authorize controller SSH key on the server
  authorized_key:
    user: '{{ admin_username }}'
    key: "{{ lookup('file', '{{ playbook_dir }}/data/secrets/{{ host_fqdn }}.id_rsa.pub') }}"
    manage_dir: yes

- name: create auth-done status file
  template: src=var_lib_srv01_auth-done.j2 dest=/var/lib/srv01/setup-done owner=root group=root mode=0600
