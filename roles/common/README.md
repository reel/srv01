common
=============

This role will install/configure various base system components:

- Configurable ([SFTP](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol)) user accounts for secure file transfer, protected by SSH key authentication. These users are granted access to a single shared directory.
- Firewall with [FireHOL](https://en.wikipedia.org/wiki/Firehol)
- Bruteforce protection with `fail2ban`
- Automatic daily/weekly/monthly backups of user data with ([Rsnapshot](https://rsnapshot.org/)) (local backup)
- Automatic security upgrades and restart notifications
- Hostname, date/time and NTP setup
- Hardened sysctl configuration
- APT package management
- File/cron/resource permissions/limits
- Hardened PAM and logins configuration
- Hardened OpenSSH server (remote admin access + file transfer)
- Ansible facts (server state information scripts)
- Removal of unwanted base debian packages
- Removal deprecated components from previous `srv01` versions
- Basic ansible playbook requirements
- A small set of command-line tools for interactive use/debugging
- A local home page (on the cotroller) with shortcuts/URLs of installed services and a recap of admin usernames/passwords
- Basic server status analysis/diagnostic/reporting script. `#TODO Move to netdata role`

[![](https://i.imgur.com/fKkrKrJ.png)](https://i.imgur.com/tyG7Yu9.png)
[![](https://i.imgur.com/ZxPqqJs.png)](https://i.imgur.com/viqdoAK.png)


Clients
------------

SSH/SFTP file transfer clients:

 * Linux: any file manager ([Thunar](http://docs.xfce.org/xfce/thunar/start)/[Nautilus](https://wiki.gnome.org/action/show/Apps/Nautilus)/[Dolphin](https://www.kde.org/applications/system/dolphin/)) + `openssh-client`
 * Windows: [WinSCP](https://winscp.net/eng/index.php), PuTTY


Notes
-------------

**Backups size:** `rsnapshot` deduplicates unchanged files using hardlinks, but changing a single byte in a file will cause this file to be backed up again, increasing the total backup size. rsnapshot is also not aware of file moves/renamed files - they will be considered as new files, increasing the backup directory size.

**libvirt Directory sharing:** If running in a libvirt VM, to share a directory from the host/hypervisor to the VM: display the VM settings in ` virt-manager`, click `Add hardware > Filesystem`, Mode: `Mapped` , Source path: `/path/to/the/directory/to/share`, Target path: `/exampleshareddirectory`, then inside the VM run `sudo mount -t 9p /exampleshareddirectory /mnt/example`.

**Troubleshooting:** If the program gets stuck on `TASK [setup] *****`, you probably entered a wrong SUDO password. (https://github.com/ansible/ansible/issues/12608)

Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)


Dependencies
------------

None

Example Playbook
----------------


License
-------

GNU GPLv3


References
-----------------

- https://gitlab.com/nodiscc/NOTES/blob/master/config-management.md#ansible
- https://gitlab.com/nodiscc/NOTES/blob/master/storage-filesystems-databases.md#backups
- https://gitlab.com/nodiscc/NOTES/blob/master/debian.md#apt
- https://gitlab.com/nodiscc/NOTES/blob/master/networking.md#firewall
- https://gitlab.com/nodiscc/NOTES/blob/master/monitoring.md
- https://gitlab.com/nodiscc/NOTES/blob/master/ssh.md
