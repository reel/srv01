tt-rss
=============

This role will install [Tiny Tiny RSS](https://en.wikipedia.org/wiki/Tiny_Tiny_RSS), a web-based [News feed reader](https://en.wikipedia.org/wiki/News_aggregator) and aggregator.

It allows you to subscribe to many blogs/websites updates using the [RSS](https://en.wikipedia.org/wiki/RSS) standard, and access all news in a single, unified application. It comes with extensive fiiltering/sorting/searching/presentation options, and plugins to integrate multimedia content from subscribed pages.

* **[Homepage](https://tt-rss.org/)**
* **[Community/forums/support](https://discourse.tt-rss.org/)**

[![](https://i.imgur.com/UoKs3x1.png)](https://i.imgur.com/yDozQPU.jpg)
[![](https://i.imgur.com/7oO67Xq.png)](https://i.imgur.com/rNTiRva.png)
[![](https://i.imgur.com/CqoOfXo.png)](https://i.imgur.com/mv2fppi.jpg)

Clients
------------

Tiny Tiny RSS can be accessed through a web browser. Other applications can be used:

- Android: [TTRSS-Reader](https://f-droid.org/repository/browse/?fdid=org.ttrssreader)
- Linux: [Liferea](https://lzone.de/liferea/)

Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)


Dependencies
------------

None

Example Playbook
----------------


License
-------

GNU GPLv3


References
-----------------

- **https://en.wikipedia.org/wiki/RSS**
- **https://en.wikipedia.org/wiki/Tiny_Tiny_RSS**
- https://github.com/isaacsimmons/ansible-role-ttrss
- https://github.com/marklee77/ansible-role-ttrss
- https://github.com/marklee77/ansible-role-ttrss/
- https://github.com/skottler/ttrss-ansible/
- https://www.commentcamarche.net/faq/3339-agregateurs-rss-lecteurs-de-fils-rss [fr] 
