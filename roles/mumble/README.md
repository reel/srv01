mumble
=============

This role will install [Mumble](https://en.wikipedia.org/wiki/Mumble_(software)) server, a voice chat ([VoIP](https://en.wikipedia.org/wiki/Voice_over_IP)) server. It is primarily designed for use by gamers and can replace commercial programs such as TeamSpeak or Ventrilo.

Mumble [clients](https://wiki.mumble.info/wiki/Main_Page) are available many computer/mobile operating systems (Windows/OSX/Linux/iOS/Android)

[![](https://i.imgur.com/jYSU9zC.png)](https://i.imgur.com/S5Z6IEw.png)


Clients
------------

- [Desktop client (Linux/OSX/Windows)](https://wiki.mumble.info/wiki/Main_Page#Download_Mumble)
- Android: [Plumble](https://f-droid.org/en/packages/com.morlunk.mumbleclient/)

Requirements
------------

This role requires Ansible 2.7 or higher.


Role Variables
--------------

See [defaults/main.yml](defaults/main.yml)

Dependencies
------------

None

Example Playbook
----------------


License
-------

GNU GPLv3
