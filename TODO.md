## TODO

_More todo-list items can be found with `git grep -r TODO`_

------------------------------------

## Roadmap for v 1.1

- Write SSH config to `~/.ssh/config` on the controller
- Add a monitoring system (see below)
- Add a `--shell` flag to `srv01` (open an interactive SSH session)
- Add a `--web` flag to `srv01` (open the webserver homepage in a browser)
- Add a `--info` flag to `srv01` (open the local HTML homepage in a browser)


### Monitoring system

Features:

- Checks
- Collection
- Graphing
- Alerting
  - Alerts dashboard
  - Daily report
  - Weekly report
  - Mail/IM

Measurements:

- I/O, I/O wait
- Process count
- Avergae load
- Disks list
- Disks space
- Disks SMART status
- Disks mdadm/RAID status
- Temperatures
- System info (inxi)
- LVM info (pvs, vgs, lvs)
- Backup status
- Services status
- Installed roles
- User account info
- PAM/SSH Login info info
- Debsums/checksums/AIDE
- Date/time
- Uptime
- Cron runs
- Package changes
- Fail2ban bans/ignores/errors, bans by jail
- Apache errors/notices/warns
- Iptables logged packets
- Debsecan vulnerabilities by type
- Lynis warnings
- RkHunter warnings
- Needrestart status
- Active network connections/connected hosts/ports
- Network traffic up/down
- CPU usage
- Memory usage
- Swap usage
- Network configuration, ARP scan
- Tiger reports
- Vuls reports https://packages.debian.org/sid/main/vuls
- spectre-meltdown-checkes reports
- links to command outputs
- `deborphan --guess-all` reports
- samba users/pdbedit -L

Tools:

- SNMP
- Nagios https://shaarli.flibidi.net/?searchterm=nagios
- Icinga2
- Cacti
- Grafana https://shaarli.flibidi.net/?searchterm=grafana
- LibreNMS
- Smokeping
- Collectd
- Statsd
- Prometheus https://github.com/improbable-eng/thanos https://github.com/cortexproject/cortex https://packages.debian.org/jessie-backports/prometheus https://packages.debian.org/jessie-backports/prometheus-node-exporter https://shaarli.flibidi.net/?searchterm=prometheus https://kjanshair.github.io/2018/07/26/prometheus-alert-manager/
- Graphite
	- Carbon
	- Whisper
- Syslog + Graylog + Elasticsearch https://en.wikipedia.org/wiki/Elasticsearch https://www.reddit.com/r/graylog/
- Zabbix https://en.wikipedia.org/wiki/Zabbix https://packages.debian.org/buster/zabbix-agent https://shaarli.flibidi.net/?searchterm=zabbix
- Icinga https://www.howtoforge.com/tutorial/how-to-install-icinga2-on-debian-9/
- TICK https://www.influxdata.com/time-series-platform/ https://www.digitalocean.com/community/tutorials/how-to-monitor-system-metrics-with-the-tick-stack-on-centos-7 https://www.digitalocean.com/community/tutorials/how-to-monitor-system-metrics-with-the-tick-stack-on-ubuntu-16-04 https://github.com/influxdata/TICK-docker https://dzone.com/articles/running-the-tick-stack-on-a-raspberry-pi
	- Telegraf: https://www.influxdata.com/time-series-platform/telegraf/ https://github.com/influxdata/telegraf  
	- InfluxDB: https://www.influxdata.com/time-series-platform/influxdb/ https://github.com/influxdata/influxdb https://www.influxdata.com/ https://en.wikipedia.org/wiki/InfluxDB https://www.docs.influxdata.com/influxdb/v1.7/ https://packages.debian.org/stretch/influxdb https://packages.debian.org/stretch/influxdb-client https://github.com/novaquark/ansible_influxdb https://docs.influxdata.com/influxdb/v1.6/guides/downsampling_and_retention/ https://docs.ansible.com/ansible/latest/modules/influxdb_database_module.html https://docs.ansible.com/ansible/devel/modules/influxdb_user_module.html
	- Kapacitor: https://www.influxdata.com/time-series-platform/kapacitor/ https://github.com/influxdata/kapacitor https://docs.influxdata.com/kapacitor/v1.5/introduction/getting-started/
		- Chronograf: https://www.influxdata.com/time-series-platform/chronograf/ https://github.com/influxdata/chronograf https://docs.influxdata.com/chronograf/v1.7/introduction/getting-started/
- Netdata: https://github.com/netdata/netdata
- Sensu/Uchiwa https://shaarli.flibidi.net/?searchterm=sensu
- Loki https://grafana.com/loki#about
- Goaccess
- RRDTool
- MRTG

----------------------------------------------------



## Todo


* [backlog] [security] setup fail2ban for mumble-server logins

```
[murmur]
# AKA mumble-server
port     = 64738
action   = %(banaction)s[name=%(__name__)s-tcp, port="%(port)s", protocol=tcp, chain="%(chain)s", actname=%(banaction)s-tcp]
           %(banaction)s[name=%(__name__)s-udp, port="%(port)s", protocol=udp, chain="%(chain)s", actname=%(banaction)s-udp]
logpath  = /var/log/mumble
```


* [backlog] [security] strong password policy: complexity, aging (warning only?/lynis), restrict use of previous passwords: pam-passwdqc:

```
######## pam_passwdqc.j2

Name: passwdqc password strength enforcement
Default: yes
Priority: 1024
Conflicts: cracklib
Password-Type: Primary
Password:
requisite pam_passwdqc.so min=disabled,disabled,16,12,8
```

- Use ansible from pip?
- install apt-transport-https before copying APT sources.list

--------------------------------------------------------------------

* [backlog] Automate playbook deployment testing
  * https://github.com/mrlesmithjr/ansible-netdata/commit/840ba74f3a2534cb064ea71afcab9181fea40227#diff-10af77051759c5d2ef5b1ebb9438ad53 https://github.com/sovereign/sovereign/blob/master/tests.py https://github.com/tobidope/ansible-tt-rss/blob/master/.travis.yml https://github.com/tobidope/ansible-tt-rss/blob/master/tests/test.yml https://github.com/dev-sec/ansible-ssh-hardening/pull/205#issuecomment-482984155
  * Unit tests? (mock external dependencies, network resources,...)


* [backlog] [security] implement whitelist/blocklist based network filtering (ipset)
  * whitelist mode: only allow IPs from specific ranges (eg country level)
  * use `ipset` which allows to efficiently manage large blocklists
  * install the `firehol-tools` package
  * ipsets can be updated using the `update-ipsets` command
  * only block inbound traffic at first, using `firehol_level*` lists
  * in the future, enable more lists
  * the `iprange` command can be used to optimize overlapping blocklists
  * https://wiki.archlinux.org/index.php/Ipset http://blog.ls20.com/securing-your-server-using-ipset-and-dynamic-blocklists/ http://iplists.firehol.org/ http://nemgeek.blogspot.fr/2013/07/ipset-for-heavy-use.html https://blather.michaelwlucas.com/archives/1679 http://serverfault.com/questions/778831/how-to-block-an-attack-on-wordpress-using-ufw-on-ubuntu-server https://github.com/firehol/blocklist-ipsets https://github.com/firehol/blocklist-ipsets/wiki/Downloading-IP-Lists https://github.com/firehol/blocklist-ipsets/wiki/Extending-update-ipsets https://github.com/firehol/blocklist-ipsets/wiki/Installing-update-ipsets https://github.com/firehol/blocklist-ipsets/wiki/Linux-iptables-ipsets https://github.com/firehol/blocklist-ipsets/wiki/Monitoring-update-ipsets https://github.com/firehol/firehol/blob/master/contrib/ipset-apply.sh https://github.com/firehol/iprange/wiki https://packages.debian.org/search?keywords=iprange  https://packages.debian.org/sid/firehol-tools http://www.ipdeny.com/ http://www.ipdeny.com/ipblocks/data/countries/ http://www.linuxjournal.com/content/advanced-firewall-configurations-ipset http://xmodulo.com/block-unwanted-ip-addresses-linux.html https://github.com/amatas/ansible-iptables/ https://everythingshouldbevirtual.com/automation/ansible-ip-sets-and-dshield-block-list/ https://github.com/mrlesmithjr/ansible-ipset, https://github.com/dannysheehan/iptables-ipset-blacklists https://firehol.org/guides/ipset/ https://github.com/firehol/blocklist-ipsets https://github.com/firehol/firehol/wiki/dnsbl-ipset.sh https://iplists.firehol.org/
  * manual blacklist file

* [backlog] [security] IDS/IPS?
  * https://en.wikipedia.org/wiki/Host-based_intrusion_detection_system
  * https://en.wikipedia.org/wiki/Host-based_intrusion_detection_system_comparison
  * https://en.wikipedia.org/wiki/Intrusion_detection_system
  * * suricata? https://en.wikipedia.org/wiki/Suricata_(software) https://github.com/StamusNetworks/SELKS https://redmine.openinfosecfoundation.org/projects/suricata/wiki/Basic_Setup https://suricata.readthedocs.io/en/suricata-4.0.4/ https://www.hackingarticles.in/configure-suricata-ids-ubuntu/
  * snort? https://en.wikipedia.org/wiki/Snort_(software)
  * portsentry? https://wiki.debian-fr.xyz/Portsentry https://wiki.netbsd.org/nsps/portsentry.conf https://www.linuxjournal.com/article/4751 https://www.techrapid.co.uk/wp-content/cache/page_enhanced/www.techrapid.co.uk/raspberry-pi/avoid-port-scanning-portsentry-raspberry-pi/_index.html http://www.computersecuritystudent.com/UNIX/UBUNTU/1204/lesson14/ 
  * tripwire? https://en.wikipedia.org/wiki/Open_Source_Tripwire
  * OSSEC? https://en.wikipedia.org/wiki/OSSEC

* [backlog] automate disk partitioning
  - swap: https://jvns.ca/blog/2017/02/17/mystery-swap/ https://www.redhat.com/en/about/blog/do-we-really-need-swap-modern-systems https://help.ubuntu.com/community/SwapFaq https://access.redhat.com/solutions/103833 https://access.redhat.com/solutions/15244 https://wiki.archlinux.org/index.php/Swap
  - disk encryption https://blog.imirhil.fr/2017/07/22/stockage-chiffre-serveur.html
  - RAID https://www.smbitjournal.com/2014/07/comparing-raid-10-and-raid-01/ https://serverfault.com/questions/685289/software-vs-hardware-raid-performance-and-cache-usage https://serverfault.com/questions/339128/what-are-the-different-widely-used-raid-levels-and-when-should-i-consider-them https://en.wikipedia.org/wiki/Nested_RAID_levels https://en.wikipedia.org/wiki/RAID https://en.wikipedia.org/wiki/Standard_RAID_levels http://www.linuxhomenetworking.com/wiki/index.php/Quick_HOWTO_:_Ch26_:_Linux_Software_RAID https://serverfault.com/questions/145319/is-there-a-difference-between-raid-10-10-and-raid-01-0 http://xmodulo.com/setup-raid10-linux.html
  - LVM https://askubuntu.com/questions/161279/how-do-i-move-my-lvm-250-gb-root-partition-to-a-new-120gb-hard-disk https://opensource.com/business/16/9/linux-users-guide-lvm https://sysadmincasts.com/episodes/27-lvm-linear-vs-striped-logical-volumes https://we.riseup.net/debian/setting-up-raid+crypto+lvm-by-hand-howto https://wiki.archlinux.org/index.php/LVM https://www.debian-fr.org/raid5-lvm-avant-l-installation-ajout-de-grub-pc-t48224.html#p483774 http://www.gigahype.com/resize-luks-encryped-lvm-partition/ http://xmodulo.com/2014/09/create-software-raid1-array-mdadm-linux.html http://xmodulo.com/2014/09/manage-lvm-volumes-centos-rhel-7-system-storage-manager.html
  - Other filesystems https://en.wikipedia.org/wiki/XFS https://opensource.com/article/18/4/ext4-filesystem
* [backlog] [security] detect out of date web applications
  if installed application version is < version in versions.yml, display a notification
  to make sure admin machine versions.yml is up-to-date: on each srv01 run, write date to last_run temp file in repository (gitignore it), if last_run is older than 1 day, compare current commit hash with commit hash in master branch of https://github.com/nodiscc/srv01, if hashes differ, show a warning message and ask user to git pull
* [backlog] [security] debsums/rkhunter integrity checks are too long, overlap
   * split them to separate srv01-utils integrity command
   * run weekly (cron)
   * better integrity checks: update valid checksums from config files in the repo

#### Unsorted

* [backlog] [feature] <https://github.com/kucrut/ttrss-reader>
* [backlog] [feature] add bash completion to srv01 script <https://iridakos.com/tutorials/2018/03/01/bash-programmable-completion-tutorial.html>
* [backlog] srv01: use python-yaml or a template file to create _all_ config files
* [backlog] [feature] analytics (piwik...)
* [backlog] [feature] backups: after backup: if configured backup drive is plugged in, display debug message, mount it and sync contents of the backup directory to it (add doc on preparing the usb drive, automount: usbmount?)
* [backlog] [feature] bittorrent tracker https://gitlab.com/dessalines/torrents.csv
* [backlog] [feature] client: autosave important data to local directory (tt-rss opml, shaarli public/private/all, nextcloud files/calendar/tasks/contacts (webdav/caldav/carddav?); luks keys, certs...)
* [backlog] [feature] CUPS printer sharing
* [backlog] [feature] distributed filesystem (ceph, glusterfs)
* [backlog] [feature] FTP server (vsftpd)
* [backlog] [feature] gui: dialog --clear --checklist text 100 24 80 tag_name command_name off; clear
* [backlog] [feature] HTTP/HTTPS caching proxy (squid)
* [backlog] [feature] https://packages.debian.org/buster/cockpit system Web console
* [backlog] [feature] hypervisor (KVM+libvirt, Proxmox VE?)
* [backlog] [feature] mail server dma/nullmailer/opensmtpd/postfix/exim4/sendmail/ssmtp https://www.reddit.com/r/linuxadmin/comments/8x7ll3/selfhosting_mail/ https://www.ers.trendmicro.com/reputations https://www.linode.com/docs/email/
* [backlog] [feature] Main RSS feed on the homepage: Aggregate feeds from links, images, nextcloud shared files, blog, bugs.... (https://github.com/cogdog/feed2js?)
* [backlog] [feature] maps/routing web applications: provide directions/routes on a map: (Graphhopper: search/autocompletion/points/fast/just JRE and a .jar file/reverseproxy/needs an OSM file)
* [backlog] [feature] maps/routing: local tileserver (rendering and autodownload of .osm needed)
* [backlog] [feature] mariadb https://github.com/linuxserver/docker-mariadb
* [backlog] [feature] Minetest server http://www.minetest.net/ https://github.com/linuxserver/docker-minetest
* [backlog] [feature] mumble Auto-setup accounts on client
* [backlog] [feature] Other confg management (chef,puppet,vagrant...)
* [backlog] [feature] pad/etherpad
* [backlog] [feature] portainer
* [backlog] [feature] postgresql (tt-rss, nextcloud)
* [backlog] [feature] pulseaudio: support rtp/sonos streaming https://gist.github.com/denysvitali/6d15e0cab8c4851d76ca19356d30be05
* [backlog] [feature] RADIUS server
* [backlog] [feature] Reaplce `ntp` with `chrony`? https://chrony.tuxfamily.org/comparison.html https://fedoraproject.org/wiki/Features/ChronyDefaultNTP https://www.thegeekdiary.com/centos-rhel-7-chrony-vs-ntp-differences-between-ntpd-and-chronyd/ https://wiki.archlinux.org/index.php/Chrony NTP server (LAN only)
* [backlog] [feature] SANE network scanner server + client setup documentation
* [backlog] [feature] Search engine (Searx)
* [backlog] [feature] setup firefox send https://blog.adminrezo.fr/2017/08/firefox-send-sur-debian-stretch-transfert-de-fichiers-chiffres-libre-et-gratuit/
* [backlog] [feature] setup https://github.com/ziahamza/webui-aria2
* [backlog] [feature] setup wallabag
* [backlog] [feature] simple git server? https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server http://kovshenin.com/2011/howto-remote-shared-git-repository/
* [backlog] [feature] ssl/ssh multiplexing (serve all services on port 443 - sslh)
* [backlog] [feature] apache: ability to add custom links to homepage
* [backlog] [feature] apache: ability to add custom title to homepage
* [backlog] [feature] apache: ability to add custom message to homepage
* [backlog] [feature] apache: ability to create custom directories under files/
* [backlog] [feature] ubiquity unfi ap controller (branch `feature/ubiquiti-controller`)
* [backlog] [feature] UPNP mediaserver
* [backlog] [feature] video streaming platform (OBS + https://github.com/arut/nginx-rtmp-module?) https://gitlab.com/Deamos/flask-nginx-rtmp-manager https://serverfault.com/questions/168211/apache-as-a-front-end-to-red5
* [backlog] [feature] wake on lan https://wiki.debian.org/WakeOnLan
* [backlog] [feature] Working playbooks for debian, centos, freeBSD
* [backlog] [feature]beets http://beets.io/
* [backlog] [feature]let services setup NAT rules using upnp (miniupnpc, minissdpd - on startup, every time firewall rules are changed, periodically)
* [backlog] [security] [firewall] build firehol policy from snippets in each role, only allow ports required by installed roles
* [backlog] [security] aide/tripwire/iontegrity checker
* [backlog] [security] automate upgrade availability detection: display warning if available version > configured version
* [backlog] [security] disable compilers (optional): chmod 000 /usr/bin/gcc /usr/bin/cc
* [backlog] [security] generic login banners
* [backlog] [security] https://observatory.mozilla.org/ C+ (CSP) https://www.html5rocks.com/en/tutorials/security/content-security-policy/
* [backlog] [security] https://securityheaders.io/ add HSTS and Refferer-Policy, move unsafe-inline CSP to per-app config
* [backlog] [security] implement STIGs STIG https://github.com/MindPointGroup/RHEL6-STIG https://github.com/MindPointGroup/RHEL7-CIS https://linux.die.net/man/8/pam_faillock https://github.com/MindPointGroup/RHEL6-STIG/blob/devel/tasks/prelim.yml#L202 https://www.cisecurity.org/cis-benchmarks/
* [backlog] [security] install and configure auditd https://sematext.com/blog/auditd-logs-auditbeat-elasticsearch-logsene/ https://www.dsfc.net/logiciel-libre/linux/centos-linux-logiciel-libre/configurer-auditd-sur-centos-7/ https://github.com/dev-sec/ansible-os-hardening/blob/master/templates/etc/audit/auditd.conf.j2 https://github.com/Neo23x0/auditd
* [backlog] [security] mount /var/ noexec?
- [feature] https://github.com/kucrut/ttrss-reader
* [backlog] [security] reports: add https://github.com/nbs-system/php-malware-finder. REQUIRES yara 3.4+ package (debian stretch)
* [backlog] [security] reports: run https://github.com/speed47/spectre-meltdown-checker/ https://security-tracker.debian.org/tracker/CVE-2017-5754 https://en.wikipedia.org/wiki/Meltdown_(security_vulnerability)
* [backlog] [security] restrict use of su
* [backlog] [security] restrict use of system resources, prevent forkbombs (ulimit/limits.conf)
* [backlog] [security] run --propupd when the system is known to be in a clean state
* [backlog] [security] sandboxing/RBAC/service isolation (apparmor + libapache2-mod-apparmor?)
* [backlog] [security] setup a "tripwire/honeypot" decoy directory on the webserver (eg /admin/), permaban any IP attempting to access it. Optionally serve a zip bomb https://blog.haschek.at/2017/how-to-defend-your-website-with-zip-bombs.html
* [backlog] [security] setup process accounting (acct package, commands: ac, sa; check /var/log/wtmp)
* [backlog] add a blogging engine/static site generator (jekyll: https://jvns.ca/blog/2014/10/08/how-to-set-up-a-blog-in-5-minutes/)
* [backlog] add logrotate rules for srv01-utils
* [backlog] cleanup: more cleanup in logs (*.0,*.1.log,)
* [backlog] doc: screenshots (out of the repository)
* [backlog] dokuwiki: conf/local.php: `$conf['youarehere'] = 1;` <https://github.com/linuxserver/docker-dokuwiki>
* [backlog] fetch important logs during sync (/var/log/lynis-report.dat )
* [backlog] mail: use external SMTP provider (package dma, dma.conf, smarthost, ssmtp, nullmailer, msmtp-mta, masqmail...)
* [backlog] p2p filesharing: instant.io, reep.io, filepizza
* [backlog] remove tiger/... crons and let srv01-utils handle periodic scans
* [backlog] report: check backups integrity (https://github.com/bit-team/backintime/wiki/FAQ#id24)
* [backlog] report: display LAN/WAN IP addresses
* [backlog] [security] SELinux https://www.reddit.com/r/linuxadmin/comments/42ykum/how_widely_is_selinux_actually_used/ https://www.cyberciti.biz/faq/rhel-fedora-redhat-selinux-protection/
* [backlog] [security] SCAP workbench https://www.open-scap.org/security-policies/ https://static.open-scap.org/openscap-1.2/oscap_user_manual.html https://static.open-scap.org/ https://www.open-scap.org/tools/openscap-base/ https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sect-using_oscap `openscap-scanner scap-security-guide scap-workbench`
* [backlog] support extended ACLs
* [backlog] web server: ability to publish a pub/ files directory: create symlink to /var/www/html in user directory, set proper permissions (sgid)
* [easy] [security] [feature] srv01-utils: add optional https://github.com/nodiscc/consoleSSLlabs test, or https://github.com/drwetter/testssl.sh
* [enh] [security] Detect files with SUID bit set `find / -xdev -perm -4000 -o -perm -2000 -type f ! -path '/proc/*' -print 2>/dev/null`, https://github.com/CISOfy/lynis/issues/558
* [enh] automatic pfSense configuration https://www.netgate.com/docs/pfsense/monitoring/copying-logs-to-a-remote-host-with-syslog.html https://forum.netgate.com/topic/80368/syslog-ng-tls-configuration-help-2-1-5an https://support.oneidentity.com/technical-documents/syslog-ng-premium-edition/7.0.9/administration-guide/ remote syslog (syslog-ng) https://github.com/amatas/ansible-pfsense https://github.com/overdrive3000/ansible-pfsense https://github.com/opoplawski/ansible-pfsense/ https://www.netgate.com/docs/pfsense/development/using-the-php-pfsense-shell.html https://www.blog-libre.org/2015/08/29/connexion-ansible-sur-pfsense-nas-qnap-et-synology/
* [reports] add a script to compare installed packages against a whitelist, warn user if a non-whitelisted package is installed
* [reports] config chkrootkit https://en.wikipedia.org/wiki/Chkrootkit
* [reports] echo -e "\n #### Visits time range: \n";
* [reports] firewall hits
* [reports] https://github.com/derhansen/logwatch-modsec2
* [reports] ncdu? tree?
* [reports] sync to client and include extra logs: goaccess, logwatch, /var/log/lynis-report.dat, /var/log/lynis.log, rkhunter, tiger
* [reports] tiger: `tiger -E -q`, edit `/etc/tiger/tigerrc` et tiger.ignore
* [test] [security] SFTP users are chrooted https://devtidbits.com/2011/06/29/implement-a-sftp-service-for-ubuntudebian-with-a-chrooted-isolated-file-directory/
* [feature] IPBX/SIP Server https://www.reddit.com/r/selfhosted/comments/aphkl7/any_way_of_purchasing_a_pstn_number_and_doing_the/
* [backlog] GDPR compliance? https://www.reddit.com/r/selfhosted/comments/apuatn/gdpr_for_my_selfhosted_services/
* [backlog] [feature] Guacamole https://blog.adminrezo.fr/2018/01/guacamole-un-proxy-web-dacces-distants/

#### Common/base

* [enh] install 9mount (optional) only if ansible_virtualization_platform = kvm
* [enh] install haveged only when ansible_virtualization_platform is not null
* [enh] mail sender via a smtp relay/smarthost (dma? ssmtp?)
* [enh] Alternative reconfiguration of timezone?

```yaml
- name: timezone - reconfigure tzdata
  command: dpkg-reconfigure --frontend noninteractive tzdata

- name: set timezone
  file:
    src: /usr/share/zoneinfo/Europe/Paris
    dest: /etc/localtime
    force: yes
    state: link
```

* [enh] check locale generation (/etc/locale.gen? locale_gen ansible module?)
* [enh] ssh: add molly-guard?
* [enh] [security] restrict su binary access to root:root? (`file: dest='/bin/su' owner=root group=root mode=0750`)
* [enh] [security] minimize write access to directories? (this causes a "maximum recursion depth exceeded" error - number of files?):

```yaml
- file: path='{{ item }}' mode='go-w' recurse=yes
  with_items:
    - '/usr/local/sbin'
    - '/usr/local/bin'
    - '/usr/sbin'
    - '/usr/bin'
    - '/sbin'
    - '/bin'
    - '/home'
```


#### Git/project managament

- [enh] gitea: continuous integration: https://drone.io/ https://jenkins.io/ https://circleci.com/ https://www.digitalocean.com/community/tutorials/ci-cd-tools-comparison-jenkins-gitlab-ci-buildbot-drone-and-concourse
- [backlog] [feature] Gitlab https://en.wikipedia.org/wiki/GitLab + Gitlab CI https://www.reddit.com/r/gitlab https://thelastcicada.com/2016/running-gitlab-in-docker/ https://docs.gitlab.com/omnibus/docker/ https://hub.docker.com/r/gitlab/gitlab-ce/ https://github.com/adejoux/gitlab_ansible_docker https://docs.ansible.com/ansible/latest/modules/docker_image_module.html https://docs.ansible.com/ansible/latest/modules/docker_container_module.html https://stackoverflow.com/questions/46195852/how-to-run-gitlab-in-docker-container-with-nginx-proxy-over-ssl-with-letsencrypt https://gist.github.com/netdesk/c1db2985b542f9916995139318e5a7ce https://hub.docker.com/r/jwilder/nginx-proxy/ https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion/ https://docs.gitlab.com/ce/administration/environment_variables.html https://docs.gitlab.com/omnibus/settings/ssl.html https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template

##### Icecast

* [enh] icecast: make playlist storage directory configurable
* [enh] icecast: allow streaming from a source client running on another machine https://github.com/quodlibet/quodlibet/issues/2795#issuecomment-451708898
* [enh] icecast: allow streaming other formats (mp3)
* [enh] icecast: update stream title metadata with currently playing track
* [enh] icecast: provide a fallback audio file in case the playlist is empty


##### Wontfix

* [wontfix] [security] port knocking? (knockd/fwknop) - provides only minimal advantage (security through obscurity - we use secure SSH key-only authentication/root logins are disabled) and adds another point of failure over SSH


##### VPN

* [backlog] [feature] [security] OpenVPN Server
 http://my1.fr/blog/creer-son-serveur-vpn-sous-debian/ http://openvpn.net/index.php/open-source/documentation/howto.html http://www.tecmint.com/install-openvpn-in-debian/ https://angristan.fr/installer-facilement-serveur-openvpn-debian-ubuntu-centos/ https://carmagnole.ovh/tuto-utiliser-protonvpn-simplement.htm https://en.wikipedia.org/wiki/OpenVPN https://github.com/Angristan/OpenVPN-install https://github.com/git-sgmoore/OpenVPN_Ipsec_L2tp_server_on_Digital_Ocean https://github.com/linuxserver/docker-vpn https://github.com/Nyr/openvpn-install https://github.com/sovereign/sovereign/tree/master/roles/vpn https://github.com/StarshipEngineer/OpenVPN-Setup https://gitlab.com/braincoke/blog/blob/blog/network/_posts/2017-12-10-configure-an-openvpn-server-on-a-tomato-router.markdown https://networkfilter.blogspot.fr/2015/01/be-your-own-vpn-provider-with-openbsd.html https://openvpn.net/index.php/open-source.html https://openvpn.net/index.php/open-source/documentation/howto.html https://we.riseup.net/debian/personal-vpn https://wiki.archlinux.org/index.php/Easy-RSA https://wiki.archlinux.org/index.php/OpenVPN https://wiki.debian.org/OpenVPN https://wiki.debian.org/OpenVPN https://github.com/Stouts/Stouts.openvpn
* [backlog] [security] firewall: add a mode to restrict services access to VPN only <https://serverfault.com/questions/451788/how-to-access-a-port-via-openvpn-only>
* [backlog] [feature] IPSEC VPN? https://en.wikipedia.org/wiki/IPsec https://raymii.org/s/tutorials/IPSEC_vpn_with_Ubuntu_16.04.html https://blog.ls20.com/ipsec-l2tp-vpn-auto-setup-for-ubuntu-12-04-on-amazon-ec2/
* [backlog] [feature] SSH VPN? https://help.ubuntu.com/community/SSH_VPN https://wiki.archlinux.org/index.php/VPN_over_SSH

##### Samba

* [backlog] [security] protect samba from password bruteforce attempts from LAN (fail2ban)
* [backlog] [security]  protect samba from cryptolockers (fail2ban, http://techtalk.pcpitstop.com/2016/09/15/54418/...)
* [backlog] [security] samba: establish a blacklist (or whitelist) of filetypes to reject from being shared (exe...) using veto files/delete veto files config directives
* [backlog] [security] https://www.samba.org/samba/docs/man/Samba-HOWTO-Collection/securing-samba.html
* [backlog] advertise samba over avahi http://www.macdweller.org/2012/05/13/samba-bonjour-with-avahi/ http://www.macdweller.org/2012/05/13/samba-bonjour-with-avahi/ https://www.howtoforge.com/tutorial/samba-debian-9-stretch-server/
* [backlog] additional cleanup tasks for samba?
* [backlog] samba file quotas: https://man.cx/quotatool
* [backlog] [feature] LDAP + PostgreSQL backend, Samba4 as AD: FREEIPA http://www.freeipa.org/page/Main_Page; libpam-ldapd ldapscripts libpam-ldapd, https://wiki.debian.org/DebianLAN, https://packages.debian.org/jessie/debian-lan-config, openldap, http://fai-project.org/, PGINA https://pgina.org/ https://pgina.org/docs/v3.1/ldap.html https://github.com/turnkeylinux-apps/domain-controller https://help.ubuntu.com/lts/serverguide/samba-dc.html https://wiki.samba.org/index.php/Setting_up_Samba_as_an_Active_Directory_Domain_Controller https://www.howtoforge.com/centos-5.x-samba-domain-controller-with-ldap-backend-p2

##### Nextcloud

* [backlog] [feature] nextcloud: add notes, documents, deck, https://rello.github.io/audioplayer/, https://github.com/nextcloud/circles, https://github.com/pawelrojek/nextcloud-drawio/, https://github.com/icewind1991/files_markdown, https://github.com/nextcloud/groupfolders, https://github.com/nextcloud/news, https://github.com/jsxc/jsxc/wiki/Install-ojsxc-(owncloud), https://apps.nextcloud.com/apps/registration
* [backlog] [bug] [test] nextcloud occ cleanup does not work
* [backlog] [security] https://scan.nextcloud.com/
* [backlog] [feature] add https://apps.nextcloud.com/apps/workflow_script
* [enh] add nextcloud maintenance on/off switch
* [backlog] [feature] https://github.com/owncloud/bookmarks/releases.atom
* [enh] music: use version from https://github.com/paulijar/music/releases.atom instead of hardcoded version in nextcloud.yml
* [enh] radio app https://git.project-insanity.org/onny/nextcloud-app-radio/tags
* [enh] [security] nextcloud download verification

```yaml
- name: download nextcloud checksums
 get_url:
   url=https://download.nextcloud.com/community/nextcloud-{{ nextcloud_version }}.zip.sha256
   dest=/root/nextcloud-{{ nextcloud_version }}.zip.sha256

- name: verify nextcloud zip checksums
 command: sha256sum -c /root/nextcloud-{{ nextcloud_version }}.zip

- name: download nextcloud release GPG key
 get_url:
   url=https://download.nextcloud.com/community/nextcloud-x.y.z.zip.asc
   dest=/root/nextcloud-{{ nextcloud_version }}.zip.asc

- name: download nextcloud main GPG key
 get_url:
   url=https://www.nextcloud.com/nextcloud.asc
   dest=/root/nextcloud.asc

- name: import GPG key
 command: gpg --import /root/nextcloud.asc

- name: verify nextcloud zip GPG signature
 command: gpg --verify /root/nextcloud-{{ nextcloud_version }}.zip.asc /root/nextcloud-{{ nextcloud_version }}.zip
```


##### Instant messaging/communication

Modern chat platform with E2E encryption, 1-to-1 or conference text/audio/video chat, file transfer, markdown support, mobile clients

- **https://matrix.org/**
  - https://matrix.org/docs/projects/try-matrix-now.html
- **https://rocket.chat/**
  - https://forums.rocket.chat/
  - https://github.com/RocketChat/Rocket.Chat/issues/4161
- **https://nextcloud.com/talk/** - https://github.com/ReinerNippes/nextcloud/blob/master/roles/prep_talk/tasks/Debian.yml
  - https://apps.nextcloud.com/apps/spreed
  - https://nextcloud.com/blog/rocket.chat-and-nextcloud-announce-partnership-and-integration/
- **https://www.jsxc.org/**
- **https://www.mattermost.org/**
  - https://github.com/nextcloud/server/issues/3202
- https://jitsi.org/jitsi-meet/
- https://www.spreed.me/?nlr
- https://github.com/versatica/mediasoup
- https://jami.net/
- https://www.reddit.com/r/selfhosted/comments/6po5k6/slack_alternative_with_file_storage_email/
- https://www.reddit.com/r/selfhosted/comments/7k471o/zulip_vs_rocketchat_vs_mattermost_vs_riotim_slack/
- https://www.reddit.com/r/selfhosted/comments/7wyyrn/mattermost_versus_matrix_which_is_better/
- https://www.reddit.com/r/selfhosted/comments/989768/matrixriot_vs_rocketchat/
- https://www.reddit.com/r/selfhosted/comments/9i8c3f/matrixriot_vs_mattermost/
- https://www.reddit.com/r/selfhosted/comments/9s5fzq/team_chat_recommendation_mattermost_riot_zulip/
- https://www.slant.co/topics/4554/~open-source-alternatives-to-slack
- https://www.maketecheasier.com/mattermost-vs-rocketchat/
- https://stackshare.io/stackups/mattermost-vs-rocketchat-vs-slack
- https://www.akitaonrails.com/2016/08/13/choosing-mattermost-over-rocket-chat-and-slack
- https://www.inboundhow.com/slack-vs-mattermost-rocket-chat-open-source-alternatives-worth/
- https://project-management.zone/system/mattermost,nextcloud
- https://project-management.zone/system/mattermost,rocket-chat
- https://autoize.com/rocketchat-vs-mattermost/
- https://meta.wikimedia.org/wiki/Research_on_open_source_team_communication_tools


##### Apache/Web server

* [apache] `web_password_protected_dirs`: add an `autoindex` variable for each dir (yes/no). Apache will allow the directory to be indexed with mod_autoindex if set to yes.
* [apache] Add cutsomizable footer message to homepage (opt-in)
* [apache] Add title to homepage (opt-out, default to fqdn)
* [apache] only install base apache in apache role, let applications require specific php5+extensions
* [apache] remove hpkp?
* [apache] switch apache to HTTP2
* [apache] use apache mod\_md to manage letsencrypt certificates https://letsencrypt.org/2017/10/17/acme-support-in-apache-httpd.html https://github.com/icing/mod_md>
* [backlog] apache: add default /var/www/public directory, add backup rule, add symlink in user dir
* [feature] allow using nginx instead of apache/switch to nginx + php-fpm https://github.com/trimstray/nginx-quick-reference https://en.wikipedia.org/wiki/Nginx https://www.linode.com/docs/web-servers/nginx/how-to-configure-nginx/ https://www.nginx.com/ https://nginx.org/en/docs/beginners_guide.html https://blog.adminrezo.fr/2015/02/cacher-version-apache-nginx-php-fcgi/ https://docs.nextcloud.com/server/latest/admin_manual/installation/nginx.html https://nginx.org/en/docs/http/configuring_https_servers.html
* [security] [apache] improve SSL configuration, reach Class A grade on SSL tests
* [security] [apache] install and configure apache modsecurity2 +modsecurity-crs for Debian Stretch https://modsecurity.org/crs/ (other waf? nginx waf?), use fail2ban apache-modsecurity filter to ban hosts causing alerts
* [security] [apache] restrict access to login/admin urls based on host ip address

##### Shaarli

* [enh] set shaarli timezone/user/password...
* [enh] use new json config format


##### DNS/DHCP Server

* [backlog] [feature] add a command/cron job to auto-update freedns.afraid.org DNS records with the latest WAN IP
* [backlog] [feature] bind9 authoritative DNS
* [backlog] [feature] DHCP+TFTP/PXE server (udhcpd? dnsmasq? isc-dhcp-server?) https://www.linuxstall.com/setting-up-your-linux-box-to-serve-as-a-dhcp-server-or-a-dhcp-client/ https://djlab.com/2014/10/debian-pxe-boot-image-from-scratch/ https://docs.oracle.com/cd/E20815_01/html/E20821/gjqeh.html https://docs.oracle.com/html/E50247_08/vmiug-install-pxe-pxelinux.html https://askubuntu.com/questions/445653/how-to-configure-pxelinux-cfg-default-properly-in-order-to-install-ubuntu-from-n https://wiki.centos.org/HowTos/PXE/PXE_Setup/Menus https://www.syslinux.org/old/pxe.php https://www.syslinux.org/wiki/index.php?title=PXELINUX https://debian-handbook.info/browse/stable/sect.automated-installation.html https://www.evanjones.ca/software/pxeimager-scratch.html https://old.reddit.com/r/linuxadmin/comments/b47hez/bare_metal_provisioning_inmemory_os/ https://en.wikipedia.org/wiki/Preboot_Execution_Environment
* [backlog] [security] DNS based filtering: https://filterlists.com/ https://github.com/collinbarrett/FilterLists https://github.com/StevenBlack/hosts https://pi-hole.net/ + manual whiteslist/blacklist + /etc/hosts + dedpulication
* [enh] allow setting custom DNS records in dnsmasq https://liquidat.wordpress.com/2017/03/03/howto-automated-dns-resolution-for-kvmlibvirt-guests-with-a-local-domain/
* DNS load testing: https://github.com/pi-hole/dnsblast
* Hosts/DNS blocking mechanism: nullroute? route to 0.0.0.0? iptables DROP/REJECT?

##### Database

* [backlog] [security] do not store mysql root credentials in plain text in /root/my.cnf
* [backlog] [security] add a read-only mysql user for backups https://gitlab.com/braincoke/blog/blob/blog/backup/_posts/2017-08-06-mysql-backup.markdown
* [enh] [security] update mysql root password for all root accounts - eg run dpkg-reconfigure mysql-server-5.5

```
# this does not work
- mysql_user:
    name: root
    host: "{{ item }}"
    password: "{{ mysql_password }}"
    priv: "*.*:ALL,GRANT"
  with_items:
    - "{{ ansible_hostname }}"
    - 127.0.0.1
    - ::1
    - localhost
```

```

```
* [enh] [security]: secure mysql installation - make it non-interactive: https://gist.github.com/Mins/4602864

```
# does not work, interactive
- command: mysql_secure_installation
  notify: restart mysql
```




### Monitoring

* [enh] [security] logwatch: detect entries in other-vhosts-access.log
* [enh] [security] setup tiger
* [enh] setup goaccess
* [enh] [security] enforce passwd file permissions root/root/0644
* [bug] firefox stops playing when ezstream skips track
* [enh] Ability to load a HTML file as homepage footer (eg search/bookmarks startpage...
* [enh] add ./srv01 tasks <command> command, list tasks for a command
* [enh] compare config_version in config.yml and defaults.yml, warn and propose manually merging (meld?) files if versions differ
* [enh] doc: recommend separate /var and /tmp partitions
* [bug] fetch goaccess html report only for report/visitors, display message https://stackoverflow.com/questions/33649308/conditional-notify-in-ansible-depending-on-roles#33965922
* [enh] [security] purge NFS stack packages: nfs-kernel-server nfs-common portmap rpcbind autofs
* [enh] add global/common handlers to display warning messages/generic notifications, run them after *ALL* tags
 * alert: backups have not been synced to this machine since {{ last_backup_sync (local fact }}. Last backups found on server: {{ last_backup_time (server fact }}
 * alert: there are warnings in security reports
 * handler: show warnings in terminal (debug: messages)
 * handler: show warning notifications (local action: notify-bin message)
 * improve docker role (firewall: https://github.com/firehol/firehol/issues/114)