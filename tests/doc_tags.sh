#!/bin/bash
set -o errexit
set -o nounset

cp tests/srv01-playbook.doc-tags.yml srv01-playbook.doc-tags.yml
tags=$(ansible-playbook srv01-playbook.doc-tags.yml -i srv01-inventory.yml.dist --list-tags 2>/dev/null | grep --only-matching -E '\[.*\]' | sed 's|[,[]||g' | sed 's/]//g');
never_tags=$(ansible-playbook srv01-playbook.doc-tags.yml -i srv01-inventory.yml.dist --list-tags --tags=never 2>/dev/null | grep --only-matching -E '\[.*\]' | sed 's|[,[]||g' | sed 's/]//g');
rm srv01-playbook.doc-tags.yml

echo '### TAGS ###'

comment_indent=16

for tag in $tags; do
	description=$(grep "^$tag " tests/tags.txt | cut -d " " -f1 --complement)
	taglength=${#tag}
	numspaces=$(( comment_indent - taglength ))
	spaces=$(for i in $(seq $numspaces); do echo -n ' '; done)
	echo -e "${tag}${spaces}# $description"
done

echo -e "\n-------------\n"

comment_indent=20

for tag in $never_tags; do
	description=$(grep "^$tag " tests/tags.txt | cut -d " " -f1 --complement)
	taglength=${#tag}
	numspaces=$(( comment_indent - taglength ))
	spaces=$(for i in $(seq $numspaces); do echo -n ' '; done)
	echo -e "${tag}${spaces}# $description"
done