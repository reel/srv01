# Configuration variables

### roles/apache2/defaults/main.yml :
```yaml
---

##### APACHE WEB SERVER #####

##### APACHE WEB SERVER #####
# yes/no: start/stop the apache webserver, enable/disable it on boot
apache_enable_service: yes

# Get a SSL/TLS certificate from Let's Encrypt, or generate self-signed, untrusted cert
# one of: letsencrypt, selfsigned
apache_cert_mode: 'selfsigned'
# domain name (FQDN) for generated Let's Encrypt certificates
letsencrypt_domain: "{{ host_fqdn }}"
# e-mail address used for Let's Encrypt TOS agreement
letsencrypt_account_email: "{{ admin_email }}"

# List of web server directories to protect with basic username/password authentication
# Relative to the webserver document root ( "private" -> https://my.server.com/private/)
# No trailing slash!
# Run ./srv01 apache-update to apply changes.
apache_password_protected_dirs: [ ]
#  - { dir: "private", user: "myusername", password: "p@$$w0rd" } # example


##### MYSQL/MARIADB DATABASE SERVICE #####

# yes/no: start/stop the mysql service, enable/disable it on boot
mysql_enable_service: yes

# hostname for the mariadb database service
mysql_hostname: "{{ host_fqdn }}"
# root password for the mariadb database service
mysql_root_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.mysql_root_password length=15') }}"

```

### roles/common/defaults/main.yml :
```yaml
---

##### SYSTEM #####
# The fully qualified domain name for the server
#host_fqdn: 'CHANGEME'

# Linux administrator user account name/password
# User must be part of the 'sudo' group
#admin_username: 'CHANGEME'

# Password for the administrator account. Do not lose or reuse this password.
#admin_password: 'CHANGEME'

# TCP port for SSH server
ssh_port: '823'
# user for SSH/SFTP access
ansible_ssh_user: "{{ admin_username }}"
# password for first SSH access, to configure SSH keys
ansible_ssh_pass: "{{ admin_password }}"
# sudo password
ansible_sudo_password: "{{ admin_password }}"

# Port for ansible SSH connections
ansible_ssh_port: '{{ ssh_port }}'

##### SSH/SFTP #####
# User accounts to create for SFTP-only access, along with their SSH key password
# These users will be restricted to file operations in a single shared directory
# If you need to delete an account, you must do it manually on the server (sudo deluser old_user)
sftp_users: []
#  - { name: "bob", key_passphrase: "AxiN3td8hqsVSB4rpE0" } # example


##### BACKUPS #####
# Path to local backups directory on the server
backups_dir: '/var/backups/srv01'
# Additional directories to include in backups
backup_important_dirs: []
  #- '/media/'    # example
  #- '/home/git/' # example
# Number of daily backup generations to keep
backups_retain_daily: 6
# Number of weekly backup generations to keep
backups_retain_weekly: 4
# Number of monthly backup generations to keep
backups_retain_monthly: 6



#### REPORTS/MONITORING #####
report_run_debsums: 'yes' # yes/no: run debsums during reports (long)
report_run_lynis: 'yes' # yes/no: run lynis during reports (long)
report_run_rkhunter: 'yes' # yes/no: run rkhunter during reports (long)
report_run_smart: "yes" # yes/no: query SMART during reports (useless if server is a VM)
report_logwatch_days: "7" # time range for logwatch report (number of days)
report_validity_hours: "24" # time in hours after which the locally downloaded reports will be considered out-of-date
reports_diskusage_warning_threshold: '80' # disk usage percent above which a warning is sent
reportviewer: 'xdg-open' # local program used to read reports #TODO move local logic to main script

# Whether to update rkhunter file integrity database
# Only set to True on a known clean system!
rkhunter_update_integrity_database: False


##### FIREWALL #####

# Interface/networks/zones definitions
# The default configuration assumes a single physical network interface
# This interface is connected to both the LAN and Internet (through a gateway)
# This will work for most home setups where the machine is behind a NAT
# LAN and Internet traffic are guessed from their source/destination address
# If you have separate network cards for LAN and Internet access, you can edit the "interface"
# directive in the networks below.

firehol_networks:
  - name: "lan1"
    src: "{{ ansible_default_ipv4.address }}/16"
    # traffic matching the default IPv4 LAN will match these rules
    # if no rules in this block match, rules for the 'internet' interface will be used
    interface: "any"
    policy: "RETURN"
    allow_input:
      - "pulseaudio" # required by pulseaudio role
      - "samba" # required by samba role
      - "mdns" # required by pulseaudio IF avahi announcement is enabled
      - "multicast" # required by pulseaudio IF avahi announcement is enabled
      - "sshcustom" # anti lock-out rule, always allow SSH from LAN
      - "ping" # allow ping from LAN
    allow_output:
      - "dhcp"
  - name: "internet"
    # traffic that does not match any LAN address will use these rules
    src: "any"
    interface: "any"
    policy: "RETURN"
    allow_input:
      - "http" # required by apache role
      - "https" # required by apache role
      - "mumble" # required by mumble role
      - "ssh" # allow SSH on default port 22 from anywhere (can be removed after initial deployment/SSH port change)
      - "sshcustom" # allow SSH on custom port from anywhere
      - "transmission" # required by transmission role
    allow_output:
      - "dns"
      - "ntp"
      - "http" # required by common role + most web applications
      - "https" # required by common role + most web applications
      - "all" # required by transmission role (remote dynamic ports)

# Custom firehol services definitions
firehol_services:
  - { name: "sshcustom", server_port: "tcp/{{ ssh_port }}", client_port: "default" } # OpenSSH server on custom port
  - { name: "mumble", server_port: "tcp/64738 udp/64738", client_port: "default" } # Mumble VoIP server
  - { name: "transmission", server_port: "tcp/52943", client_port: "any" } # Transmission bittorrent client (peer connections).  # bittorrent remote ports are dynamic high ports, allowing all outgoing traffic is required if you run transmission
  - { name: "mdns", server_port: "udp/5353", client_port: "default" } # Avahi zero-configuration multicast DNS/DNS-SD/service publication
  - { name: "pulseaudio", server_port: "tcp/4713", client_port: "default" } # Pulseaudio network sound server

# yes/no: don't ban IP addresses from the default network
fail2ban_ignore_default_network: yes

# don't ban IP addresses from these networks
fail2ban_ignoreip: []
```

### roles/docker/defaults/main.yml :
```yaml
---

# Edition can be one of: 'ce' (Community Edition) or 'ee' (Enterprise Edition).
docker_edition: 'ce'
docker_package: "docker-{{ docker_edition }}"
docker_package_state: present

# Docker Compose options.
docker_install_compose: true
docker_compose_version: "1.22.0"
docker_compose_path: /usr/local/bin/docker-compose

# Used only for Debian/Ubuntu. Switch 'stable' to 'edge' if needed.
docker_apt_release_channel: stable
docker_apt_arch: amd64
docker_apt_repository: "deb [arch={{ docker_apt_arch }}] https://download.docker.com/linux/{{ ansible_distribution|lower }} {{ ansible_distribution_release }} {{ docker_apt_release_channel }}"
docker_apt_ignore_key_error: true

# Used only for RedHat/CentOS/Fedora.
docker_yum_repo_url: https://download.docker.com/linux/{{ (ansible_distribution == "Fedora") | ternary("fedora","centos") }}/docker-{{ docker_edition }}.repo
docker_yum_repo_enable_edge: 0
docker_yum_repo_enable_test: 0

# A list of users who will be added to the docker group.
docker_users: []

# yes/no: start/stop docker service, enable/disable it on boot
docker_enable_service: yes```

### roles/gitea/defaults/main.yml :
```yaml
---

####### GITEA

#username/password/e-mail address for the gitea admin user
gitea_admin_user: "{{ services_username }}"
gitea_admin_password: "{{ services_password }}"
gitea_admin_email: "{{ admin_email }}"

#Password for the gitea SQL database
gitea_db_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.gitea_db_password length=15') }}"

# yes/no: start/stop the gitea service, enable/disable it on boot
gitea_enable_service: yes

#last/private/public: Default privacy setting when creating a new repository
gitea_default_private: "private"
#true/false: disable third-party gravatar service
gitea_disable_gravatar: "true"
#true/false: Disallow registration, only allow admins to create accounts.
gitea_disable_registration: "true"
#true/false: enable/disable gitea REST API
gitea_enable_api: "false"
#true/false: force every new repository to be private
gitea_force_private: "false"
#true/false: disable all third-party/external services/CDNs
gitea_offline_mode: "true"
#true/false: User must sign in to view anything.
gitea_require_signin: "true"
#true/false: whether the email of the user should be shown in the Explore Users page
gitea_show_user_email: "false"
#Global limit of repositories per user, applied at creation time. -1 means no limit
gitea_user_repo_limit: "-1"
#64 CHARACTER global secret key
gitea_secret_key: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.gitea_secret_key length=64') }}"


# version (git tag)
# https://github.com/go-gitea/gitea/releases.atom; remove leading v
gitea_version: '1.8.0'
```

### roles/gitlab-runner/defaults/main.yml :
```yaml
---

######## GITLAB-RUNNER

# Maximum number of jobs to run concurrently
gitlab_runner_concurrent: '{{ ansible_processor_cores }}'
# GitLab coordinator URL
gitlab_runner_coordinator_url: 'https://gitlab.com/ci'
# GitLab registration token
gitlab_runner_registration_token: ''
# Runner description
gitlab_runner_description: 'Gitlab Runner on {{ ansible_hostname }}'
# Runner executor
gitlab_runner_executor: 'shell'
# Default Docker image
gitlab_runner_docker_image: ''
# Runner tags
gitlab_runner_tags: []
```

### roles/icecast/defaults/main.yml :
```yaml
---

######## ICECAST AUDIO STREAMING SERVER #####

#Password required to send a stream to the icecast server.
icecast_source_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.icecast_source_password length=15') }}"


# yes/no: start/stop the icecast media streaming server, enable/disable it on boot
icecast_enable_service: yes

# Output OGG stream bitrate (in kB/s)
icecast_bitrate: '128'

# icecast stream metadata Name
icecast_info_name: '{{ host_fqdn }}'
# icecast stream metadata Homepage/URL
icecast_info_url: 'https://{{ host_fqdn }}'

# Username/password for web interface administration. Unused but must be set
icecast_admin_username: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.icecast_admin_username length=15') }}"
icecast_admin_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.icecast_admin_password length=15') }}"

# Password to connect an icecast relay. Unused but must be set
icecast_relay_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.icecast_relay_password length=15') }}"
```

### roles/mumble/defaults/main.yml :
```yaml
---

######## MUMBLE VoIP SERVER
# NO SPACES! PUBLIC password to join the Mumble voice server
mumble_server_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.mumble_server_password length=15') }}"
# NO SPACES! PRIVATE Admin password to mumble server (login as 'superuser')
mumble_superuser_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.mumble_superuser_password length=15') }}"


# yes/no: start/stop the apache webserver, enable/disable it on boot
mumble_enable_service: yes
```

### roles/nextcloud/defaults/main.yml :
```yaml
---

##### NEXTCLOUD #####

# Nextcloud default/admin username/password
nextcloud_user: '{{ services_username }}'
nextcloud_password: '{{ services_password }}'
nextcloud_fqdn: '{{ host_fqdn }}'

#nextcloud data storage directory
nextcloud_data_dir: "/var/nextcloud/data"

# nextcloud database credentials
nextcloud_db_name: "nextcloud"
nextcloud_db_user: "nextcloud"
nextcloud_db_table_prefix: "oc_"
nextcloud_db_host: "localhost"
nextcloud_db_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.nextcloud_db_password length=15') }}"


##### VERSIONS #####

# https://github.com/nextcloud/server/releases.atom, remove leading v
nextcloud_version: '15.0.7'
# https://github.com/nextcloud/calendar/releases.atom
nextcloud_calendar_version: 'v1.6.4'
# https://github.com/owncloud/bookmarks/releases.atom #TODO
nextcloud_bookmarks_version: 'v0.16.3' #TODO
# https://github.com/nextcloud/contacts/releases.atom
nextcloud_contacts_version: 'v3.1.0'
# https://github.com/paulijar/music/releases.atom #TODO hardcoded in nextcloud.yml
# https://git.project-insanity.org/onny/nextcloud-app-radio/tags #TODO
nextcloud_radio_version: '0.6.0' #TODO
# https://github.com/nextcloud/tasks/releases.atom
nextcloud_tasks_version: 'v0.9.8'
# https://github.com/nextcloud/spreed/releases.atom, remove leading v
nextcloud_talk_version: '5.0.3'

##### APPLICATIONS #####

nextcloud_apps:
  - { state: "disable", app: "encryption" }
  - { state: "disable", app: "files_antivirus" }
  - { state: "disable", app: "files_versions" }
  - { state: "disable", app: "news" }
  - { state: "disable", app: "user_external" }
  - { state: "disable", app: "user_ldap" }
  - { state: "enable", app: "activity" }
  - { state: "enable", app: "contacts" }
  - { state: "enable", app: "comments" }
  - { state: "enable", app: "dav" }
  - { state: "enable", app: "federatedfilesharing" }
  - { state: "enable", app: "federation" }
  - { state: "enable", app: "files" }
  - { state: "enable", app: "files_external" }
  - { state: "enable", app: "files_pdfviewer" }
  - { state: "enable", app: "files_sharing" }
  - { state: "enable", app: "files_texteditor" }
  - { state: "enable", app: "files_trashbin" }
  - { state: "enable", app: "files_videoplayer" }
  - { state: "enable", app: "firstrunwizard" }
  - { state: "enable", app: "gallery" }
  - { state: "enable", app: "notifications" }
  - { state: "enable", app: "systemtags" }
  - { state: "enable", app: "tasks" }
  - { state: "enable", app: "updatenotification" }
  - { state: "enable", app: "music" }
  - { state: "enable", app: "spreed" }
#  - { state: "enable", app: "documents" } #TODO requires download
#  - { state: "enable", app: "templateeditor" } #TODO requires download
```

### roles/pulseaudio/defaults/main.yml :
```yaml
---

##### PULSEAUDIO #####

# yes/no: auto-configure the ansible controller as a pulseaudio client (fetch the cookie)
pulseaudio_configure_local_cookie: no

# yes/no: start/stop the pulseaudio sound server, enable/disable it on boot
pulseaudio_enable_service: yes

# number of audio output channels on the server's soundcard (2 for stereo, 6 for 5.1...)
pulseaudio_output_channels: '2'

# yes/no: enable publishing the pulseaudio sink over avahi/mDNS
# this will cause pulseaudio clients on the same LAN to auto-detect the server
# as available output sink (provided they have the same pulse cookie)
pulseaudio_enable_mdns_publishing: 'yes'
```

### roles/shaarli/defaults/main.yml :
```yaml
---

# https://github.com/shaarli/Shaarli/releases.atom
shaarli_version: 'v0.10.4'
```

### roles/transmission/defaults/main.yml :
```yaml
---

######## BITTORRENT CLIENT
# username for remote control web interface
# password for remote control web interface
transmission_username: '{{ services_username }}'
transmission_password: '{{ services_password }}'

# yes/no: start/stop the transmission bitorrent client, enable/disable it on boot
transmission_enable_service: yes

# Torrents download directory for
transmission_download_dir: '/var/lib/transmission-daemon/downloads'

# TCP/UDP port used for incmoing bittorrent peer connections
transmission_port: '55702'
```

### roles/tt-rss/defaults/main.yml :
```yaml
---

# tt-rss main user/admin username/password
tt_rss_user: "{{ services_username }}"
tt_rss_password: "{{ services_password }}"

# tt-rsss version (git)
tt_rss_version: "master"

# fully qualified domain name for this tt-rss instance
tt_rss_fqdn: "{{ host_fqdn }}"

# password for tt-rss mysql database
tt_rss_db_password: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.tt_rss_db_password length=15') }}"

# random 24 string used for encryption of passwords for password-protected feeds
tt_rss_feed_crypt_key: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.tt_rss_feed_crypt_key length=24') }}"
# RANDOM 8 CHARACTERS string used to salt the password
tt_rss_password_salt: "{{ lookup('password', playbook_dir + '/data/secrets/' + host_fqdn + '.tt_rss_password_salt length=8') }}"

# yes/no: whether to install extra themes for tt-rss
# defaults to no, the default theme is alright
ttrss_install_themes: no
```

