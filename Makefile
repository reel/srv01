#!/usr/bin/env make
SHELL := '/bin/bash'

all: tests

# Required packages: ansible ansible-doc python3-venv
tests: syntax_check pylint isort ansible_lint yamllint

# Create a python virtual environment with all required tools
venv:
	python3 -m venv $$HOME/.venv && pip3 install ansible ansible-lint yamllint pylint isort

# Run ansible in syntax check mode
syntax_check: venv
	cp tests/srv01-playbook.test.yml srv01-playbook.test.yml
	ansible-playbook srv01-playbook.test.yml -i tests/srv01-inventory.test.yml --syntax-check
	rm srv01-playbook.test.yml

# Python linter
pylint: venv
	source $$HOME/.venv/bin/activate && pylint --rcfile=tests/pylintrc srv01

# Sort python imports
isort: venv
	source $$HOME/.venv/bin/activate && isort --check-only srv01

# Ansible linter
ansible_lint: venv
	source $$HOME/.venv/bin/activate && ansible-lint srv01-playbook.yml.dist || exit 0

# YAML syntax check and linter
yamllint: venv
	source $$HOME/.venv/bin/activate && find ./ -iname "*.yml" -exec yamllint -c tests/.yamllint '{}' \;

# generate the big list of all config variables as markdown
.PHONY: doc
doc: doc_tags
	@default_files=$$(find roles/*/defaults/ -iname "main.yml" | sort); \
	echo -e '# Configuration variables\n' >| doc/config-variables.md; \
	for file in $$default_files; do \
		echo "### $$file :"; echo '```yaml'; cat $$file; echo -e '```\n'; \
	done >> doc/config-variables.md

# output a list of tags and their descriptions
# if some tags are undocumented they will show up here
# fill descriptions in tests/tags.txt
doc_tags:
	@./tests/doc_tags.sh

# generate thumbnails for screenshots
# usage: place your screenshots in a screenshots/ directory
# and run make doc_make_thumbs. requires imagemagick, optipng
doc_make_thumbs:
	for i in screenshots/*.png; do convert -format png -resize 150x "$$i" "$$i.thumb"; done
	optipng screenshots/*